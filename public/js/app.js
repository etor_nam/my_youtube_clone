// var app = angular
//             .module('yourtube', [
//               'ngCookies',
//               'ngRoute',
//               'ngStorage',
//               'ngMessages',
//               'angularMoment',
//               'angular-loading-bar',
//               'ui.bootstrap',
//               'appRoutes',
//               'ngSanitize',
//               'toastr',
//               'ngLodash',
//               'hc.marked',
//               'angularUtils.directives.dirDisqus',
//               'satellizer'])
//   .config(['cfpLoadingBarProvider','$authProvider', function(cfpLoadingBarProvider, $authProvider){

//     $authProvider.baseUrl = '/';
//     $authProvider.loginUrl = '/api/login';
//     $authProvider.signupUrl = '/api/register';
//     $authProvider.authHeader = 'Authorization';
//     $authProvider.authToken = 'Bearer';
//     $authProvider.storageType = 'localStorage';

//     cfpLoadingBarProvider.includeSpinner   = false;
//     cfpLoadingBarProvider.includeBar       = true;

//   }]);



var app = angular
  .module('yourtube', [
    'ngCookies',
    'ngRoute',
    'appRoutes',
    'ngStorage',
    'toastr',
    'angular-cloudinary',
    'ngFileUpload',
    'satellizer',
    'mwl.confirm'])
  .config(function ($authProvider, cloudinaryProvider) {

    $authProvider.baseUrl = '/';
    $authProvider.loginUrl = '/api/login';
    $authProvider.signupUrl = '/api/register';
    $authProvider.authHeader = 'Authorization';
    $authProvider.authToken = 'Bearer';
    $authProvider.storageType = 'localStorage';

    // cfpLoadingBarProvider.includeSpinner   = false;
    // cfpLoadingBarProvider.includeBar       = true;
    // cloudinaryProvider.config({
    //   upload_endpoint: 'https://api.cloudinary.com/v1_1/', // default
    //   cloud_name: 'etornam', // required
    //   upload_preset: 'n27ppanx', // optional
    // });
  });